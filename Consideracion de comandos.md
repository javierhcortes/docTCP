##Como renombrar Archivos de firmware tip .hex

1) 
**Command**

		Peticion acceso : pet_acc
**Resumen**

		Este comando se debe implementar con un nuevo action_code en cliente
		Al recibirlo una luminaria desea entrar al sistema.
		Se podria notificar con un cambio de icono sutil.

**Mecanica**

		Cuando El server recibe un mensaje de tipo Identification Frame
		en modo ejecutar o modo Respuesta o modo Reporte, se genera el mensaje TCP ASYNC : pet_acc


2) 
**Command**

		Nueva alerta : new_alert
**Resumen**

		Este comando se debe implementar en cliente
		Para notificar de alertas que estan ocurriendo en el momento

**Mecanica**

		No hay mecanicas implementadas para generar el mensaje asincronico.
		En un futuro se implementara este mensaje cuando llegen ciertos tipos de mensajes de error
		o estados nuevos estados de mantencion. Por definir.

 3) 
**Command**

		CAmbio modo operacion : opmode_change
**Resumen**

	Solo esta probado el cambio de programa a modo 2.
	Hay que hacer pruebas de cambio de todos los modos de operacion.
	Considerar nuevos modos de operacion definidos en protocolo
	Considerar comportamiento cuando hay diferencias de informacion entre uca y base datos
	
**Mecanica**

	Este comando cambia los valores en base de datos.
	No se ha verificado el comportamiento del servidor ni sus comandos a enviar.

4) 
**Command**

		doChangeGroupProg :  group_assoc_change    = 11;
**Resumen**

		Este comando si esta implementado en el server : Revisar Comandos TCP
		"6.- Modificar grupos-programas - Tabla a modificar: programs_group__rel
		Frame TCP : 47; AckTcp; GroupID; n_add; ...; ...; n_del; ...; ..."

**Mecanica**

		Cuando algun cliente manda a ejecutar una asociacion de grupos y programas
		se envia un broadcast a todos los clientes con commando 11.
		Este comando solo se ejecutara si y solo si se recibe un comando 47 desde algun cliente
		forzando al cambio de asociacion.

5) 
**Command**
		VArios comandos : curves_add, curves_points, progr_add

**Resumen**

		Estos comandos no fueron ejecutados porque el server no proceso las instrucciones que lo generan
		Se envio una respuesta de error, pero el procedimiento conitnuo a traves del cliente. ( corregir)

**Mecanica**

		Corregir en el server el comportamiento de los comandos que generar estos asincronicos.
		Funciones a corregir : doChangeCurveAdd , doChangeCurvePoints , doChangeProgramAdd

6) 
**Command**

		Desconexion USB : usb_out
**Resumen**

		Este asincronico se debe implementar un mensaje de aviso en cliente.
		El hecho que se informe de este evento significa que no hay comunicacion al coordindador
		Todos los mensajes enviados a ese coordiador tendran un frame de error status : (por definir) SIN_USB

**Mecanica**

		Para reproducir el usb_out, iniciar cliente, desconectar usb y esperar asincronico.
		Luego tratar de enviar un comando por ese coordinador.

7) 
**Command**

		command 
**Resumen**

		Comprobar que se envie un status error, cuando el server no encuentre ese id o no hay comunicacion

**Mecanica**

		Localizar una uca apagada y forzar a cambiar su command desde el cliente

8) 
**Command**

		Exceso de conexiones : many_conecctions
**Resumen**

		El cliente se cae. Al parecer el server fuerza la desconexion close tipo2 del socket
		y produce una excepcion

**Mecanica**

		Abrir tres clientes al mismo tiempo y depurar

9) 
**Command**

		Group Command : group_command

**Resumen**

		Redefinir implementacion . Red debiera trasnportar el grupo, si no
		Definir cuando es error ( si falla 1 o todos)
