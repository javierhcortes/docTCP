# Informacion de Conexiones TCP

En este documento se describe como comunicarse al Servidor Zeus.
Existen tres tipos de conexion al ServidorZeus.

## Tipos de conexion al Servidor 3G

- Conexion TCP para clientes WEB. Puerto defecto 3025
- Conexion TCP para coordinadores 3G. Puerto defecto 16440
- Conexion TCP para depuracion de RED. Puerto defecto 17000

## Conexion TCP par clientes WEB

- Para conectarse a este tipo de conexion se debe autentificar con las credenciales
que estan almacenadas en la base de datos en la tabla user_pool

- Este tipo de conexion esta bajo encriptacion de los datos :
- Este tipo de conexion tiene un formato de comunicacion donde en un mensaje enviado "msg" :
- Los bytes msg[0] y msg[1] corresponde al largo del mensaje. msg[0] es el byte mas significativo.
- El byte msg[2] debe ser  igual a 0x41
- El byte msg[3] debe ser  igual a 0x42
- Los bytes restantes se deben desencryptar deacuerdo al algoritmo de encrptacion sicom
- para la decriptacion se utiliza la funcion Decrypt2. primer argumento el buffer de mensaje desde el byte[4] y el largo solo considerando los bytes a desencriptar.
- para desencriptar se debe utilizar ....por completar

## Conexion TCP para coordinadores 3G

- Para conectarse a este tipo de conexion, se debe autentificar con un mensaje de IdentificationFrame 3G del protocolo sicom.
- Esta conexion no tiene encryptacion, se procesan los bytes como son requeridos por el protocolo sicom.

## Conexion TCP para depuradores de red

- Para esta conexion solo se necesita conectarse al socket y se recibirarn
los mensajes de depuracion de coordinadores que esten activados para enviar mensajes de debug.

### Tipo de Mensajes de conexion TCP par clientes WEB

Existen dos tipos de mensajes en esta categoria:

- los mensajes sincronicos

    los cuales se envian esperando una respuesta inmediata

La lista de mensajes sincronicos es

- los mensajes asincronicos

    los cuales se envian sin esperar una respuesta inmediata

### Estructura de mensajes TCP

Se detalla solamente el payload del mensaje, dejando fuera los bytes 0, 1, 2 y 3 antes detallados:

- es una array de bytes, en su representacion en ascii,
- los argumentos son separados por ;
- El largo de los argumentos y cantidad es variable en funcion del comando

Ej : Comando Login (con valor 37), con AckTCP = 5, con usuario = IngSicom, con Password = IngSicom. Entonces el mensaje que deberia enviar **ANTES** de codificar queda entre []

      [37;5;IngSicom;IngSicom.]

### Listado de comandos y valores


    namespace TCP{
      const int login                 = 37;
      const int command               = 38;
      const int answer                = 44;
      const int group_command         = 45;
      const int change_mode           = 46;
      const int change_group_prog     = 47;
      const int change_group_devi     = 52;
      const int change_group_add      = 53;
      const int change_curve_add      = 54;
      const int change_curve_point    = 55;
      const int change_progr_add      = 60;
      const int change_progr_event    = 61;
      const int change_days           = 62;
      const int change_status         = 63;
      const int debug_msg             = 64;
      const int reintentar_msg_wError = 70;
      const int queryDimming_tcp      = 80;
      const int firmware_update_Aoch  = 150;
    }

    namespace TCP_response{
      const int sta_ok                = 20;
      const int sta_bad_msg           = 21;
      const int sta_bad_login         = 22;
      const int sta_double_login      = 23;
      const int sta_retry_login       = 24;
      const int sta_frame_err         = 25;
      const int sta_gar               = 26;
      const int sta_user_loged        = 27;
      const int sta_timeout           = 28;
      const int sta_msg_unknow        = 29;
      const int sta_work              = 30;
      const int index_encrypt   = 4;
    }

    namespace TCP_ASYN{
      const int pet_acc               = 2;
      const int refresh_graphics      = 8;
      const int new_alert             = 9;
      const int opmode_change         = 10;
      const int group_assoc_change    = 11;
      const int grouo_devic_change    = 16;
      const int curves_add            = 17;
      const int curves_points         = 18;
      const int progr_add             = 19;
      const int progr_curves          = 24;
      const int day_add               = 25;
      const int usb_out               = 26;
      const int many_conections       = 27;
      const int paired_dimming        = 28;
      const int status_message        = 31;
      }

### Como usar cada comando

  - login = 37

  [login; Ack Tcp; User; Pass]

  - command               = 38;

  [command; Ack Tcp; deviceID; Command type; Value]

  - answer                = 44;

  [answer; 0; Ack Tcp IdPreguntado; AckTcp(nuevo); status;]

  - group_command         = 45;

  [group_command; AckTcp; GroupId; Command; Value]

  - change_mode           = 46;

  [change_mode; AckTcp; groupID; opMode; SunSetCorrection; SunRiseCorrection; programId]

  - change_group_prog     = 47;

  [change_group_prog; AckTcp; GroupID; n_add; ...; ...; n_del; ...; ...]

  - change_group_devi     = 52;

  [change_group_devi; AckTcp; GroupId; n_Add; ...; ...; n_del; ...; ...]

  - change_group_add      = 53;

  [change_group_add; AckTcp; n_Modificacion *; GroupId; NAME; compatibilidad; DESCRIPCION;]

  - change_curve_add      = 54;

  [change_curve_add; AckTcp; n_Modificacion *; CurveId; curve_name; curve_description; cmd_id; curve_start_type; curve_start_type; curve_resolution_type; curve_resolution; curve_interpolation;]

  - change_curve_point    = 55;

  [change_curve_point; AckTcp; CurveId; NPoints; Postition1; Value1; ....;...;  PositionNPoints; ValueNPoints;]

  - change_progr_add      = 60;

  [change_progr_add; AckTcp; n_Modificacion *; ProgramId; Name; description]

  - change_progr_event    = 61;

  [change_progr_event; AckTcp; ProgramId; NEvent; DayId1; CurveId1 .... DayIdNEvent,CurveIdNEvent]

  - change_days           = 62;

  [change_days; AckTcp; n_Modificacion *; DayId; Name; Description; day_date;]

  - change_status         = 63;

  [change_status, AckTcp; new_status; nDevices; Dev1..., Devn,]

  - debug_msg             = 64;

  [debug_msg; 0; Source_Protocolo ; PAYLOAD;]

  - reintentar_msg_wError = 70;

  [reintentar_msg_wError; ackTcp; id tarea (client_msg_pool)]

  - queryDimming_tcp      = 80;

  [queryDimming_tcp; ackTcp; Device ID; Command Query]

  - firmware_update_Aoch  = 150

  [firmware_update_Aoch; ackTcp; id_firmware; device_padre; nDevices actualizar;Device1; .... ; DeviceN;]

Notas : 
* n_Modificacion : 1 -> Agregar, 2 -> Eliminar , 3-> Editar

### Servidor Zeus

Estos documentos contiene informacion de los comandos tcp enviados al server y su funcionamiento.
Estos frames estan descritos en texto plano, hay que agregarles la encriptacion.

1) ComandosTCP_desglose.xlsx

	Muestra la lista de comandos y el desglose en campos de cada uno

2) Ejemplos comandos TCP.md

	Muestra la lista de comandos con ejemplos y los valores usados en el server
	Hay algunos ejemplos rapidos de login

3)flujoTCP - server

	Diagrama de funcionamiento de la interpetacion de bytes en payload tcp para
	su reconstruccion en el servidor

4)ComandosTCP_casos Especiales

	Frames especiales que no ejecutan accion inmediatamente.
